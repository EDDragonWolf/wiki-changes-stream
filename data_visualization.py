from PyQt5.QtWidgets import QMainWindow, QLabel, QTableView, QWidget, QVBoxLayout, QHBoxLayout
from pyqtgraph.widgets.MatplotlibWidget import MatplotlibWidget
from data_model import DataTableModel, DataFilterModel
from datetime import datetime
import time


MESSAGE_COUNT_LABEL = "Messages per second: {}"


class StatisticData(object):
    def __init__(self):
        self._data = {}
        self._data_pairs = []

    def get(self, key, default_value):
        return self._data.get(key, default_value)

    def set(self, key, value):
        if key in self._data:
            for dp in self._data_pairs:
                if key == dp[0]:
                    dp[1] = value
        else:
            self._data_pairs.append([key, value])
        self._data[key] = value

    def get_scores(self, count=10, highest=True):
        sorted_values = sorted(self._data_pairs, key=lambda dp: dp[1], reverse=highest)
        res = {}
        for dp in sorted_values[:count]:
            res[dp[0]] = dp[1]
        return [res.keys(), res.values()]


def collect_frequency(frequency, data, key):
    for d in data:
        count = frequency.get(d[key], 0)
        count += 1
        frequency.set(d[key], count)
    return frequency


def user_activity(activity, data):
    return collect_frequency(activity, data, "user")


def type_activity(activity, data):
    return collect_frequency(activity, data, "type")


def server_activity(activity, data):
    return collect_frequency(activity, data, "server_name")


def human_activity(activity, data):
    return collect_frequency(activity, data, "bot")


class DataVisualizer(QMainWindow):
    def __init__(self, processor, parent=None):
        QMainWindow.__init__(self, parent)
        self.setWindowTitle("Wikipedia Changes Stream Visualizer")

        self._processor = processor
        self._processor.mpsUpdated.connect(self.update_mps_display)
        self._processor.dataUpdated.connect(self.update_data)

        self._central_widget = QWidget(self)
        self.setCentralWidget(self._central_widget)

        main_layout = QVBoxLayout()
        self._central_widget.setLayout(main_layout)

        data_layout = QHBoxLayout()
        main_layout.addLayout(data_layout)

        self._data_model = DataTableModel(self)
        self._data_model.setHeader(processor.get_data_header())

        self._filter_model = DataFilterModel(self)
        self._filter_model.setSourceModel(self._data_model)

        self._data_view = QTableView(self)
        self._data_view.setModel(self._filter_model)
        data_layout.addWidget(self._data_view)

        graph_layout = QHBoxLayout()
        main_layout.addLayout(graph_layout)

        self._user_activity = MatplotlibWidget()
        self._user_activity_subplot = self._user_activity.getFigure().add_subplot(111)
        graph_layout.addWidget(self._user_activity)

        self._human_activity = MatplotlibWidget()
        self._human_activity_subplot = self._human_activity.getFigure().add_subplot(111)
        graph_layout.addWidget(self._human_activity)

        mps_layout = QVBoxLayout()
        graph_layout.addLayout(mps_layout)

        self._message_count_label = QLabel(self)
        self._message_count_label.setText(MESSAGE_COUNT_LABEL.format(0))
        mps_layout.addWidget(self._message_count_label)

        self._mps_graph = MatplotlibWidget()
        self._mps_graph_subplot = self._mps_graph.getFigure().add_subplot(111)
        mps_layout.addWidget(self._mps_graph)

        self._mps_timestamps = []
        self._mps_value = []

    def update_mps_display(self, mps):
        self._message_count_label.setText(MESSAGE_COUNT_LABEL.format(mps))
        self._mps_timestamps.append(datetime.utcfromtimestamp(time.time()).strftime('%H:%M:%S'))
        self._mps_value.append(mps)
        self._mps_timestamps = self._mps_timestamps[-20:]
        self._mps_value = self._mps_value[-20:]
        self._mps_graph_subplot.clear()
        self._mps_graph_subplot.plot(self._mps_timestamps, self._mps_value)
        self._mps_graph_subplot.set_title('Streaming Frequency')
        self._mps_graph_subplot.set_ylabel('Messages per second (MPS)')
        self._mps_graph.getFigure().autofmt_xdate(rotation=45, ha='right')
        self._mps_graph.draw()

    def update_data(self, data):
        self._data_model.addData(data)
        raw_data = self._processor.raw_data()
        user_act = user_activity(StatisticData(), raw_data)
        max_user_act = user_act.get_scores(10)
        self._user_activity_subplot.clear()
        self._user_activity_subplot.pie(max_user_act[1], labels=max_user_act[0], autopct='%1.1f%%')
        self._user_activity_subplot.set_title("The activity of 10 most active users")
        self._user_activity.draw()

        human_act = human_activity(StatisticData(), raw_data)
        max_human_act = human_act.get_scores(10)
        readable_labels = []
        for r in max_human_act[0]:
            if r is True:
                readable_labels.append("bot")
            else:
                readable_labels.append("human")
        max_human_act[0] = readable_labels
        self._human_activity_subplot.clear()
        self._human_activity_subplot.pie(max_human_act[1], labels=max_human_act[0], autopct='%1.1f%%')
        self._human_activity_subplot.set_title("Human/Bot Activity")
        self._human_activity.draw()

    def closeEvent(self, event):
        self._processor.stop()
