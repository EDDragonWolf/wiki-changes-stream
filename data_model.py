from PyQt5 import QtCore
import time


class DataTableModel(QtCore.QAbstractTableModel):
    def __init__(self, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self._header_data = []
        self._table_data = []

    def setHeader(self, header_data):
        self._header_data = header_data
        self.headerDataChanged.emit(QtCore.Qt.Horizontal, 0, len(self._header_data))

    def headerData(self, p_int, orientation, role=None):
        if role == QtCore.Qt.DisplayRole and orientation == QtCore.Qt.Horizontal \
                and p_int < len(self._header_data):
            return self._header_data[p_int]
        else:
            return QtCore.QAbstractTableModel.headerData(self, p_int, orientation, role)

    def addData(self, data_table):
        self.beginInsertRows(QtCore.QModelIndex(), 0, len(data_table) - 1)
        self._table_data = list(reversed(data_table)) + self._table_data
        self.endInsertRows()

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self._table_data)

    def columnCount(self, parent=None, *args, **kwargs):
        return len(self._header_data)

    def data(self, index, role=None):
        if role == QtCore.Qt.DisplayRole:
            row = index.row()
            column = index.column()
            item = index.internalPointer()

            if item is not None:
                print(item)
            return self._table_data[row][column]
        return None


class DataFilterModel(QtCore.QSortFilterProxyModel):
    def __init__(self, parent=None):
        QtCore.QSortFilterProxyModel.__init__(self, parent)

    def filterAcceptsRow(self, source_row, source_parent):
        if self.sourceModel():
            index = self.sourceModel().index(source_row, 3, source_parent)
            return index.isValid() and (index.data() >= (time.time() - 100))
