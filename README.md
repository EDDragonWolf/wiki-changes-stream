# wiki-changes-stream

Simple project for collecting and visualizing data from Wikipedia Changes Stream

**Python version:** 3.6.0 or later

## Install and run

Please, install all requirements from requirements.txt file,
for example using `pip3 install -r requirements.txt`.

If you got an error with PyQt5 to try to install directly, 
like `sudo apt-get install python3-pyqt5`.

To run project, execute file `main.py`, for example, `python3 main.py`.