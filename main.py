from PyQt5.QtWidgets import QApplication
from data_visualization import DataVisualizer
from data_pipeline import DataProcessor
import sys

if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    processor = DataProcessor()
    processor.start()

    visualizer = DataVisualizer(processor)
    visualizer.show()
    
    sys.exit(app.exec_())
