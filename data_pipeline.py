from sseclient import SSEClient as EventSource
from PyQt5.QtCore import QObject, pyqtSignal
import threading
import queue
import json
import time


TABLE_HEADER = ["id", "type", "title", "timestamp", "user", "bot", "minor", "length", "server_name"]
stop_signal = False


def update_table_data(table_data, header, data):
    for d in data:
        row = []
        for h in header:
            row.append(d[h] if d.get(h, None) is not None else "")
        table_data.insert(1, row)
    return table_data


def collect_data(message_queue, processor=None):
    start = time.time()
    message_count = 0
    url = 'https://stream.wikimedia.org/v2/stream/recentchange'
    for event in EventSource(url):
        global stop_signal
        if stop_signal:
            break
        if event.event == 'message':
            try:
                change = json.loads(event.data)
            except ValueError:
                pass
            else:
                message_queue.put(change)
                message_count += 1
                end = time.time()
                elapsed_time = end - start
                if elapsed_time > 1.0 and processor:
                    processor.update_fps(message_count / elapsed_time)
                    start = end
                    message_count = 0
                processor.update_data(change)

    
class DataProcessor(QObject):
    mps_updated = pyqtSignal(float, name="mpsUpdated")
    data_updated = pyqtSignal(list, name="dataUpdated")
    data_received = pyqtSignal(name="dataReceived")

    def __init__(self, parent=None):
        QObject.__init__(self, parent)
        self._message_queue = queue.Queue()
        self._mps = 0
        self._p = threading.Thread(target=collect_data, args=(self._message_queue, self))
        self._busy = False
        self._data = []

        self.dataReceived.connect(self.process_data)
        
    def start(self):
        self._p.start()

    def stop(self):
        global stop_signal
        stop_signal = True
        self._p.join()
    
    def update_fps(self, fps):
        self._mps = fps
        self.mpsUpdated.emit(self._mps)

    def update_data(self, message):
        self._message_queue.put(message)
        if not self._busy:
            self.dataReceived.emit()

    def process_data(self):
        if not self._busy:
            self._busy = True
            table_data = []
            new_data = []
            while not self._message_queue.empty():
                new_data.append(self._message_queue.get())
            if len(new_data) > 0:
                self._data = self._data + new_data
                update_table_data(table_data, TABLE_HEADER, new_data)
                self.dataUpdated.emit(table_data)
            self._busy = False

    def raw_data(self):
        return self._data

    @staticmethod
    def get_data_header():
        return TABLE_HEADER
